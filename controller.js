const app = require('electron').remote.app;
const { remote } = require('electron');

function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
    return index == 0 ? word.toLowerCase() : word.toUpperCase();
  }).replace(/\s+/g, '');
}

$(document).ready(function () {

  const neo4j = require('neo4j-driver').v1;

  const driver = neo4j.driver('http://localhost:7474/', neo4j.auth.basic('neo4j', 'earthisflat'));

  var arvore_local = [];

  $('#salvar').click(() => {

    arvore_local = JSON.parse(localStorage.getItem('arvore'));

    const session = driver.session();
    try {

      var merge_pessoas = '';
      for (let i = 0; i < arvore_local.length; i++) {
        const p = arvore_local[i];
        merge_pessoas += `MERGE (pessoa_${i}:Pessoa {nome : '${p.pessoa}'}) `
        merge_pessoas += `MERGE (parente_${i}:Pessoa {nome : '${p.parente}'}) `
      }
      
      session.run(merge_pessoas).then((result) => {
        console.log(`Pessoas Inseridas`);

        var match_pessoas = '';
        var merge_relacionamentos = '';
        for (let i = 0; i < arvore_local.length; i++) {
          const p = arvore_local[i];
          match_pessoas += ` (pessoa_${i}:Pessoa {nome : '${p.pessoa}'}), `
          match_pessoas += ` (parente_${i}:Pessoa {nome : '${p.parente}'}),`
          merge_relacionamentos += `MERGE (pessoa_${i})-[:${p.parentesco}]->(parente_${i}) `
        }
        match_pessoas = match_pessoas.replace(/,\s*$/, "");

        session.run(
          `MATCH ${match_pessoas} ${merge_relacionamentos}`
        ).then((result) => {
          console.log(`${i} Sucesso!`);
          session.close();

          // limpa variável e tela
          arvore_local = [];
          arvore       = [];
          localStorage.removeItem('arvore');
          $('#resultado_final').empty();

        }).catch((err) => {
          throw 'Deu pau na query 2. ' + err
        });

      }).catch((err) => {
        throw 'Deu pau na query 1. ' + err
      });

    } catch (e) {
      session.close()
      throw 'DeuRuimParça# ' + e
    }

  });

  $('#incluir').click(() => {

    arvore_local.push({
      pessoa: $("#pessoa").val().toUpperCase(),
      parentesco : $("#parentesco").val().toUpperCase(),
      parentesco_text: $("#parentesco")[0].options[$("#parentesco")[0].selectedIndex].text,
      parente : $("#parente").val().toUpperCase()
    });

    console.log(arvore_local);

    var a = '<ul>',
        b = '</ul>',
        m = [];

    for (i = 0; i < arvore_local.length; i += 1){
        m[i] = '<li>' + arvore_local[i].pessoa + ' é ' + arvore_local[i].parentesco_text + ' de ' + arvore_local[i].parente + '</li>';
    }

    localStorage.setItem('arvore', JSON.stringify(arvore_local))

    $('#resultado_final').html(a + m.join(" ") + b);
  })
});